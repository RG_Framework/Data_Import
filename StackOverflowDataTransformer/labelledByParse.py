'''
This program should be executed after the tagParse execution is done.
Before running this program, please execute the following statements in PostgreSQL to create tables.
This program is to parse the labelled_by relationship of the Stack Overflow network.
--------------------------------------------------------------

create table labelled_by(
Qid int,
Tid int,
foreign key (Qid) references question (Qid),
foreign key (Tid) references tag (Tid));
--------------------------------------------------------------
@author: minjian
'''
import xml.sax
import time
import psycopg2

rcount = 0

class StackContentHandler(xml.sax.ContentHandler):
    def __init__(self):
        xml.sax.ContentHandler.__init__(self)
    
    # get values from file
    def startElement(self, name, attrs):
        # only 'row' contains elements
        if name != "row":
            return
        
        # get post type:1 is question, 2 is answer
        ptype = "null"
        if attrs.has_key("PostTypeId"):
            ptype = attrs.getValue("PostTypeId")
        
        # question's typeId is 1 
        if ptype != '1':
            return
        
        questionId = 'null'
        if attrs.has_key("Id"):
            questionId = attrs.getValue("Id")
            
        if attrs.has_key("Tags"):
            #split Tags into several parts, maximum 5 parts
            tagList = ((attrs.getValue("Tags"))[1:-1]).split("><")
            for each_tag in tagList:
                #Here is connect to your PostgreSQL
                #Change you database, user and port here
                db = "fullstackoverflow"
                dbUser = "minjian"
                dbPort = 5433
                      
                conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
                cur = conn.cursor()
                cur.execute("select tid from tag where tag_label = '" + each_tag + "';")
                rows = cur.fetchone()
                tagId = str(rows[0])
                # print out the data and echo to a txt file 
                global rcount 
                rcount += 1
                cur.execute("INSERT INTO labelled_by VALUES(%s, %s)" %
                (questionId, tagId))
                print "added ", rcount, " records into database"
                conn.commit()
                cur.close()
                conn.close()          
#Here is the path of the raw data file      
f = open('/media/minjian/OSDisk/Posts.xml')
before_time = time.time()
xml.sax.parse(f, StackContentHandler())
print("Total Data Transfer time is: ", (time.time() - before_time)/60)
f.close()