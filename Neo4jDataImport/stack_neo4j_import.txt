##This file is to show the commands that are used to export data from PostgreSQL and then inport the data into Neo4j
##@author: minjian

#The following is the commands that shoule be run in sql
COPY (SELECT * FROM answer) TO '/home/minjian/Documents/Stack_CSV/answer.csv' WITH CSV header;
COPY (SELECT * FROM question) TO '/home/minjian/Documents/Stack_CSV/question.csv' WITH CSV header;
COPY (SELECT * FROM labelled_by)  TO '/home/minjian/Documents/Stack_CSV/labelled_by.csv' WITH CSV header;
COPY (SELECT * FROM tag) TO '/home/minjian/Documents/Stack_CSV/tag.csv' WITH CSV header;
COPY (SELECT aid,parent_qid FROM answer WHERE parent_qid != 0) TO '/home/minjian/Documents/Stack_CSV/replies_for.csv' WITH CSV header;
COPY (SELECT qid,accepted_aid FROM question WHERE accepted_aid != 0) TO '/home/minjian/Documents/Stack_CSV/accept.csv' WITH CSV header;

##The following is the commands that use "neo4j-import" to import data into Neo4j 
bin/neo4j-import --stacktrace true --into /home/minjian/Downloads/neo4j-community-2.2.5/data/stack.db --id-type string --nodes:Answer /media/minjian/OSDisk/Stack_CSV/answer.csv --nodes:Question /media/minjian/OSDisk/Stack_CSV/question.csv --nodes:Tag /media/minjian/OSDisk/Stack_CSV/tag.csv --nodes:User /home/minjian/Documents/Stack_CSV/user.csv --relationships:LABELS /media/minjian/OSDisk/Stack_CSV/labelled_by.csv --relationships:ACCEPTS /home/minjian/Documents/Stack_CSV/accept.csv --relationships:REPLIES_FOR /home/minjian/Documents/Stack_CSV/replies_for.csv --relationships:ACCEPTS_USER /home/minjian/Documents/Stack_CSV/accept_user.csv

##The following is the statistics about Stack Overflow network in Neo4j
IMPORT DONE in 2m 12s 921ms. Imported:
  21713109 nodes
  31747662 relationships
  153911142 properties
4.95 GiB
There were bad entries which were skipped and logged into /home/minjian/Downloads/neo4j-community-2.2.5/data/stack.db/bad.log

